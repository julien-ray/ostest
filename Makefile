CC=gcc
CFLAGS=-I.

DEPS = 
SRC = thread.c TaskOne.c TaskTwo.c HugePage.c xattr.c rm_xattr.c
OBJ = $(SRC:.c=.o)
EXE = $(SRC:.c=)

all: $(EXE)

%: %.c $(DEPS)
	$(CC) -o $@ $< $(CFLAGS)