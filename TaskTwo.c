#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <pthread.h>

#define SHARED_MEM_SIZE 4096


void* ops(void* arg) {
    int fd = *(int*)arg;
    char* shared_mem = mmap(NULL, SHARED_MEM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (shared_mem == MAP_FAILED) {
        perror("mmap");
        exit(1);
    }
    
    // Print the data read by Thread 2
    printf("Task 2: Data read by Task 1: %s\n", shared_mem);

    sleep(5);

    // Write data to shared memory
    snprintf(shared_mem, SHARED_MEM_SIZE, "Hello from Task 2!");

    // Wait for Task 2 to read the data
    sleep(5);

    // Print the data read by Task 2
    printf("Task 1: Data given by Task 1: %s\n", shared_mem);

    //infinite loop
    while(1){
        sleep(1);
    }

    munmap(shared_mem, SHARED_MEM_SIZE);
    return NULL;
}

int main() {
    int fd = open("/dev/shmem", O_RDWR);
    if (fd == -1) {
        perror("open");
        exit(1);
    }

    ops(&fd);

    close(fd);
    shm_unlink("/dev/shmem");

    return 0;
}