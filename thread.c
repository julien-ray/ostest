#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <pthread.h>

#define SHARED_MEM_SIZE 4096

void* thread1_func(void* arg) {
    int fd = *(int*)arg;
    char* shared_mem = mmap(NULL, SHARED_MEM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (shared_mem == MAP_FAILED) {
        perror("mmap");
        exit(1);
    }

    // Write data to shared memory
    snprintf(shared_mem, SHARED_MEM_SIZE, "Hello from Thread 1!");

    // Wait for Thread 2 to read the data
    sleep(1);

    // Print the data read by Thread 2
    printf("Thread 1: Data read by Thread 2: %s\n", shared_mem);

    munmap(shared_mem, SHARED_MEM_SIZE);
    return NULL;
}

void* thread2_func(void* arg) {
    int fd = *(int*)arg;
    char* shared_mem = mmap(NULL, SHARED_MEM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (shared_mem == MAP_FAILED) {
        perror("mmap");
        exit(1);
    }

    // Wait for Thread 1 to write the data
    sleep(1);

    // Print the data written by Thread 1
    printf("Thread 2: Data written by Thread 1: %s\n", shared_mem);

    munmap(shared_mem, SHARED_MEM_SIZE);
    return NULL;
}

int main() {
    int fd = open("/dev/shmem", O_RDWR);
    if (fd == -1) {
        perror("open");
        exit(1);
    }

    pthread_t thread1, thread2;

    pthread_create(&thread1, NULL, thread1_func, &fd);
    pthread_create(&thread2, NULL, thread2_func, &fd);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);

    close(fd);
    shm_unlink("/my_shared_memory");

    return 0;
}