#!/bin/bash

command2_subprocess() {
    # Read the PID of command1
    read -r PID
    
    # Use PID in command2
    cat /proc/$PID/maps
    pmap $PID
}

# Start the first command
./thread &

# Get its PID
PID=$!

# Synchronize a variable between the two commands
# Here, we use process substitution to create a named pipe
# and use it as a way to communicate between the processes
# We pass the PID of command1 to command2 through the named pipe
{
    echo "$PID"
} > >(command2_subprocess)

# Wait for all background jobs to finish
wait

echo "All commands completed"