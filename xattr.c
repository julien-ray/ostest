#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/xattr.h>

int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf("Usage: %s <path> <name> <value>\n", argv[0]);
        return 1;
    }

    const char* path = argv[1];
    const char* name = argv[2];
    const char* value = argv[3];
    size_t size = strlen(value);

    int result = setxattr(path, name, value, size, 0);
    if (result == -1) {
        perror("Failed to set xattr");
        return 1;
    }

    printf("xattr set successfully.\n");

    // Buffer to store the attribute value
    char buffer[1024];

    // Get the attribute
    ssize_t attr_size = getxattr(path, name, buffer, sizeof(buffer));
    if (attr_size == -1) {
        perror("Failed to get xattr");
        return 1;
    }

    // Print the attribute value
    printf("xattr value: %s\n", buffer);

    return 0;
}