#include <stdio.h>
#include <sys/xattr.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/xattr.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Usage: %s <file_path> <xattr_name>\n", argv[0]);
        return 1;
    }

    const char* file_path = argv[1];
    const char* xattr_name = argv[2];

    // Remove the specified extended attribute from the file
    int result = removexattr(file_path, xattr_name);

    if (result == 0) {
        printf("Extended attribute removed successfully.\n");
    } else {
        perror("Failed to remove extended attribute");
        return 1;
    }

    return 0;
}