#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#define PAGE_SIZE (2 * 1024 * 1024)  // 2MB

int main() {
    // Allocate a huge page
    void *addr = mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB, -1, 0);
    if (addr == MAP_FAILED) {
        perror("mmap");
        return 1;
    }

    // Write to the huge page
    strcpy(addr, "Hello, HugePages!");

    // Read from the huge page
    printf("%s\n", (char *)addr);

    sleep(60);
    // Free the huge page
    if (munmap(addr, PAGE_SIZE) == -1) {
        perror("munmap");
        return 1;
    }

    return 0;
}